Module JAVA
Auteur Jordan MOLY
jordanmoly@live.fr
Projet FA3 CSV2JSON
L'application permet de convertir un fichier csv en fichier json

L'application est présentée sous la forme d'un fichier jar
Pour l'execution faire :

java -jar CSV2JSON.jar fichiercsv fichierjson 
Si le deuxième paramètre n'est pas donné, un nom est donné par défaut

Les dépendances et cycles de vie du projet sont gérées avec maven :
mvn clean install
Pour reconstruire le projet

Le versionning du code est géré avec git à l'adresse :
https://gitlab.com/jordan-moly-moly/CSV2JSON

