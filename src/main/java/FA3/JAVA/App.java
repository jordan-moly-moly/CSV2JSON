package FA3.JAVA;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


import FA3.JAVA.utils.FA3JSONGenerator;
import FA3.JAVA.utils.Fa3CSVConverter;


/**
 * Classe principale de l'application CSV2JSON.
 * 
 * @author Jordan
 * 
 */
public class App 
{
	private static final Logger trace = LogManager.getLogger(App.class);
//	PropertyConfigurator.configure("log4j.properties");
	
	/**
	 * Fonction principale de l'application
	 * @param args : Contient obligatoirement le fichier csv d'entrée et le 
	 * fichier de sortie. Si le fichier de sortie n'est pas spécifié, son nom est attribué par défaut
	 */
    public static void main( String[] args )
    {
    	if(args.length == 0){
    		trace.error("Aucun fichier spécifié en argument");
    	}
    	
    	
    	BasicConfigurator.configure();
//    	String csvFilename = "personnes.csv";
    	Fa3CSVConverter csvC = null;
    	FA3JSONGenerator jsonG = null;
    	String csvFilename = args[0];
    	FileReader fileR = null;
    	try {
    		trace.info("initialisation");
    		fileR = new FileReader(csvFilename);
    		csvC = new Fa3CSVConverter(fileR, ';', '"');
			jsonG = new FA3JSONGenerator(csvC.readAll());
			String output = null;
			if(args.length == 1){
	    		trace.error("Pas de fichier de sortie selectionné, le nom sera attribué par defaut");
	    		output=csvFilename.replace(".csv", ".json");
			if(output.equals(csvFilename)){
				output=csvFilename+".json";
			}
			}
			else{
				output = args[1];
				if(output.equals(csvFilename)){
					output=csvFilename+".json";
				}
			}
			jsonG.exportToJSONFile(output);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			trace.error("Le fichier "+csvFilename+" est introuvable", e);
//			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			trace.error("I/O exception", e);
//			e.printStackTrace();
		}
    	finally{
        		try {
        			fileR.close();
					csvC.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					trace.error("O/I exception", e);
//					e.printStackTrace();
				}
        }
    	}
    


}
