package FA3.JAVA.utils;

import java.io.IOException;
import java.io.Reader;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Cette classe est derivee de 
 * @author Jordan
 *
 */
public class Fa3CSVConverter extends CSVReader{

	public Fa3CSVConverter(Reader reader) {
		super(reader);
		// TODO Auto-generated constructor stub
	}
	
		public Fa3CSVConverter(Reader reader, char separator, char quotechar,
			boolean strictQuotes) {
		super(reader, separator, quotechar, strictQuotes);
		// TODO Auto-generated constructor stub
	}

	public Fa3CSVConverter(Reader reader, char separator, char quotechar,
			char escape, int line, boolean strictQuotes,
			boolean ignoreLeadingWhiteSpace) {
		super(reader, separator, quotechar, escape, line, strictQuotes,
				ignoreLeadingWhiteSpace);
		// TODO Auto-generated constructor stub
	}

	public Fa3CSVConverter(Reader reader, char separator, char quotechar,
			char escape, int line, boolean strictQuotes) {
		super(reader, separator, quotechar, escape, line, strictQuotes);
		// TODO Auto-generated constructor stub
	}

	public Fa3CSVConverter(Reader reader, char separator, char quotechar,
			char escape, int line) {
		super(reader, separator, quotechar, escape, line);
		// TODO Auto-generated constructor stub
	}

	public Fa3CSVConverter(Reader reader, char separator, char quotechar,
			char escape) {
		super(reader, separator, quotechar, escape);
		// TODO Auto-generated constructor stub
	}

	public Fa3CSVConverter(Reader reader, char separator, char quotechar,
			int line) {
		super(reader, separator, quotechar, line);
		// TODO Auto-generated constructor stub
	}

	public Fa3CSVConverter(Reader reader, char separator, char quotechar) {
		super(reader, separator, quotechar);
		// TODO Auto-generated constructor stub
	}

	public Fa3CSVConverter(Reader reader, char separator) {
		super(reader, separator);
		// TODO Auto-generated constructor stub
	}

	protected int nbreColonne (){
		try {
			return this.readNext().length;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
		
}
