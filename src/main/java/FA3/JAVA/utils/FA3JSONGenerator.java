/**
 * 
 */
package FA3.JAVA.utils;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONValue;

/**
 * Cette classe reçoit les informations du fichier csv et permet l'export des données
 * @author Jordan
 *
 */
public class FA3JSONGenerator {

	String[] noms_des_colonnes;
	List<String[]> donnees;
	int nbreColonnes;
	
	/**
	 * Constructeur de la classe FA3JSONGenerator
	 * @param donnees_csv : Une liste de tableau de chaine de caractère décrivant le fichier csv
	 */
	public FA3JSONGenerator(List<String[]> donnees_csv){
		if(!donnees_csv.isEmpty())
		{
			//lecture de la premiere ligne qui contient le nom des colonnes
			noms_des_colonnes = donnees_csv.get(0);
			nbreColonnes = noms_des_colonnes.length;
			donnees = donnees_csv;
			//Suppression de la ligne contenant les titres
			donnees.remove(0);
		}
	}
	
	/**
	 * Fonction d'export des données lues depuis le fichiers CSV
	 * @param pathToFile : Il donne le chemin du fichier json de sortie
	 * @return true si aucune exception rencontrée
	 */
	public boolean exportToJSONFile(String pathToFile){
		
		File output  = new File(pathToFile);
		FileOutputStream out;
		try {
			out = new FileOutputStream(output);
		
		
		OutputStreamWriter writer = new OutputStreamWriter(out);
		writer.write("{");
		for(int i = 0; i < donnees.size(); i++){
			Map<String,String> obj=new LinkedHashMap<String,String>();
			String jsonText =  null;
			jsonText="\n\t\"" + "reccord" + new Integer(i+1) + "\":";
			
			
			for(int j = 0; j < nbreColonnes; j++){
				obj.put(noms_des_colonnes[j], donnees.get(i)[j]);
			}
			
		
		  jsonText += JSONValue.toJSONString(obj);
		 
		  jsonText = jsonText.replace("{","{\n\t\t");
		  jsonText = jsonText.replaceAll(",",",\n\t\t");
		  jsonText = jsonText.replace("}","\n\t}");
		  writer.write(jsonText);
		  System.out.print(jsonText);
		}
		writer.write("\n}");
		writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
	
}
